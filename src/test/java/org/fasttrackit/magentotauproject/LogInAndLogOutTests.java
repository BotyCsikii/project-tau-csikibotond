package org.fasttrackit.magentotauproject;

import io.qameta.allure.Feature;
import org.junit.jupiter.api.DisplayName;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;

public class LogInAndLogOutTests extends Setup {

    @DisplayName("Log in attempt with incorrect Password")
    @Feature("Account")
    @Test(priority = 2)
    public void incorrectLoginPassword() {
        mainPage.accountButton.click();
        mainPage.accountLoginButton.click();
        mainPage.loginEmail.sendKeys("csikibotondi@gmail.com");
        mainPage.loginEmail.shouldHave(attribute("value", "csikibotondi@gmail.com"));
        mainPage.loginPassword.sendKeys("Test1234");
        mainPage.loginButton.click();
        $("li.error-msg").shouldHave(text("Invalid login or password."));
    }

    @DisplayName("Log in attempt with incorrect Email")
    @Feature("Account")
    @Test(priority = 3)
    public void incorrectLoginEmail() {
        mainPage.accountButton.click();
        mainPage.accountLoginButton.click();
        mainPage.loginEmail.sendKeys("csikibotondi@gmai.com");
        mainPage.loginEmail.shouldHave(attribute("value", "csikibotondi@gmai.com"));
        mainPage.loginPassword.sendKeys("Test12345");
        mainPage.loginButton.click();
        $("li.error-msg").shouldHave(text("Invalid login or password."));
    }

    @DisplayName("Log in with my account")
    @Feature("Account")
    @Test(priority = 1)
    public void login() {
        mainPage.accountButton.click();
        mainPage.myAccount.click();
        if ( $(".page-title").is(text("LOGIN OR CREATE AN ACCOUNT")) ) {
            mainPage.Login();
            mainPage.Logout();

        }

        else {
            mainPage.Logout();
            mainPage.Login();
            mainPage.Logout();
        }
    }
}
