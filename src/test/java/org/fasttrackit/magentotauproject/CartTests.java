package org.fasttrackit.magentotauproject;

import io.qameta.allure.Feature;
import org.junit.jupiter.api.DisplayName;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;

public class CartTests extends Setup {

    @DisplayName("I search for Red Dress")
    @Feature("Search")
    @Test(priority =2)
    public void search() {
        mainPage.Search("Red Dress");

    }

    @DisplayName("I check if cart is empty")
    @Feature("Cart")
    @Test(priority = 1)
    public void isCartEmpty() {
        mainPage.cartButton.click();
        $(".empty").shouldHave(text("You have no items in your shopping cart."));

    }

    @DisplayName("Add items to cart without being logged")
    @Feature("Cart")
    @Test(priority = 3)
    public void addToCartWithNoAccount() {
        mainPage.accountButton.click();
        mainPage.myAccount.click();
        $(".page-title").shouldHave(text("LOGIN OR CREATE AN ACCOUNT"));
        mainPage.Search("Red Dress");
        mainPage.addToCartBtn.get(1).click();
        mainPage.cartButton.click();
        $(".header-minicart .count").shouldHave(text("1"));
    }

    @DisplayName("Empty cart")
    @Feature("Cart")
    @Test(priority = 4)
    public void emptyCart() {
        mainPage.EmptyTheCart();
    }

    @DisplayName("Add items to cart while being logged")
    @Feature("Cart")
    @Test(priority = 5)
    public void addToCartWhileLogged() {
        mainPage.Login();
        mainPage.Search("Red Dress");
        mainPage.addToCartBtn.get(1).click();
        mainPage.cartButton.click();
        $(".header-minicart .count").shouldHave(text("1"));
        mainPage.emptyCartButton.click();
        mainPage.Logout();
    }

    @DisplayName("Verify if added items remain after login")
    @Feature("Cart")
    @Test(priority = 6)
    public void itemsAfterLogin() {
        mainPage.Search("Red Dress");
        mainPage.addToCartBtn.get(1).click();
        mainPage.cartButton.click();
        $(".header-minicart .count").shouldHave(text("1"));
        mainPage.Login();
        mainPage.cartButton.click();
        $(".header-minicart .count").shouldHave(text("1"));
        mainPage.cartButton.click();
        mainPage.EmptyTheCart();
        mainPage.Logout();

    }

    @DisplayName("Verify if cart is empty after logout")
    @Feature("Cart")
    @Test(priority = 7)
    public void noItemsAfterLogout() {
        mainPage.Login();
        mainPage.Search("Red Dress");
        mainPage.addToCartBtn.get(1).click();
        mainPage.cartButton.click();
        $(".header-minicart .count").shouldHave(text("1"));
        mainPage.Logout();
        mainPage.cartButton.click();
        $(".empty").shouldHave(text("You have no items in your shopping cart."));
    }

    @DisplayName("Verify if previously added items are in the cart upon 2nd login")
    @Feature("Cart")
    @Test(priority = 8)
    public void itemsStillInCartUponLogin() {
        mainPage.Login();
        mainPage.cartButton.click();
        $(".header-minicart .count").shouldHave(text("1"));
        mainPage.cartButton.click();
        mainPage.EmptyTheCart();
        mainPage.Logout();
    }

    @DisplayName("Change quantity of items in cart while logged out")
    @Feature("Cart")
    @Test(priority = 9)
    public void changeQuantityWithNoAccount() {
        mainPage.Search("Red Dress");
        mainPage.addToCartBtn.get(1).click();
        $(".input-text.qty[title='Qty']").click();
        $(".input-text.qty[title='Qty']").doubleClick();
        $(".input-text.qty[title='Qty']").sendKeys("10");
        $(".button.btn-update").click();
        $(".input-text.qty[title='Qty']").shouldHave(attribute("value", "10"));

    }

    @DisplayName("Remove a product while not logged")
    @Feature("Cart")
    @Test(priority = 10)
    public void removeProductWhileNotLogged() {
        mainPage.Search("Red Dress");
        mainPage.addToCartBtn.get(2).click();
        mainPage.Search("Anime");
        mainPage.addToCartBtn.get(0).click();
        mainPage.cartButton.click();
        mainPage.viewCart.click();
        mainPage.removeProductButtons.get(1).click();
        mainPage.cartButton.click();
        $(".header-minicart .count").shouldHave(text("1"));
    }

    @DisplayName("Change quantity of items in cart while logged in")
    @Feature("Cart")
    @Test(priority = 11)
    public void changeQuantityWithAccount() {
        mainPage.Login();
        mainPage.Search("Red Dress");
        mainPage.addToCartBtn.get(2).click();
        $(".input-text.qty[title='Qty']").click();
        $(".input-text.qty[title='Qty']").doubleClick();
        $(".input-text.qty[title='Qty']").sendKeys("3");
        $(".button.btn-update").click();
        $(".input-text.qty[title='Qty']").shouldHave(attribute("value", "3"));
        mainPage.Logout();
    }



    @DisplayName("Remove a product while logged in")
    @Feature("Cart")
    @Test(priority = 12)
    public void removeProductWhileLoggedIn() {
        mainPage.Search("Red Dress");
        mainPage.addToCartBtn.get(2).click();
        mainPage.Login();
        mainPage.Search("Anime");
        mainPage.addToCartBtn.get(0).click();
        mainPage.cartButton.click();
        mainPage.viewCart.click();
        mainPage.removeProductButtons.get(1).click();
        mainPage.cartButton.click();
        $(".header-minicart .count").shouldHave(text("1"));
        mainPage.Logout();
    }

    @DisplayName("Add item to wishlist while logged in")
    @Feature("Cart")
    @Test(priority = 13)
    public void addItemToWishlistWhileLoggedIn() {
        mainPage.Login();
        mainPage.Search("Blue");
        mainPage.wishListAddButtons.get(2).click();
        $(".page-title").shouldHave(text("MY WISHLIST"));
        $("li.success-msg")
                .shouldHave(text("Blue Horizons Bracelets has been added to your wishlist. Click here to continue shopping."));
        mainPage.Logout();
    }

    @DisplayName("Add item from wishlist to cart while logged in")
    @Feature("Cart")
    @Test(priority = 14)
    public void addItemFromWishlistToCart() {
        mainPage.Login();
        mainPage.accountButton.click();
        mainPage.wishListPageButton.get(1).click();
        $$(".button.btn-cart").get(1).click();
        $("li.success-msg").shouldHave(text("Blue Horizons Bracelets was added to your shopping cart."));
        mainPage.Logout();
    }
}
