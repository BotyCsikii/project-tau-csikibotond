package org.fasttrackit.magentotauproject;

import io.qameta.allure.Feature;
import org.junit.jupiter.api.DisplayName;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class AccountManagement extends Setup {

    @DisplayName("Register Account")
    @Feature("Registration")
    @Test(priority = 0)
    public void registerNewAccount() {
        mainPage.accountButton.click();
        mainPage.accountLoginButton.click();
        $("a.button").click();
        $("input#firstname").sendKeys("TestAccount");
        $("input#lastname").sendKeys("555");
        $("input#email_address").sendKeys("TestAccount555@gmail.com");
        $("input#password").sendKeys("Test12345");
        $("input#confirmation").sendKeys("Test12345");
        sleep(2000);
        $("a.back-link").click();
    }

    @DisplayName("Register Account fail")
    @Feature("Registration")
    @Test(priority = 1)
    public void failRegisterNewAccount() {
        mainPage.accountButton.click();
        mainPage.accountLoginButton.click();
        $("a.button").click();
        $("input#firstname").sendKeys("TestAccount");
        $("input#lastname").sendKeys("555");
        $("input#email_address").sendKeys("TestAccount555@gmail.com");
        $("input#password").sendKeys("Test12345");
        $("input#confirmation").sendKeys("Test1234");
        mainPage.registerButton.get(2).click();
        $("div#advice-validate-cpassword-confirmation").
                shouldHave(text("Please make sure your passwords match."));
        sleep(2000);
    }

    @DisplayName("Change Account Name")
    @Feature("AccountManagement")
    @Test(priority = 1)
    public void changeAccountName() {
        mainPage.Login();
        mainPage.editButton.get(0).click();
        $("input#middlename").sendKeys("asasd");
        $("input#current_password").sendKeys("Test12345");
        $("p~button").click();
        $("li.success-msg").shouldHave(text("The account information has been saved."));
    }
}
