package org.fasttrackit.magentotauproject;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class MainPage {

    /**
     * Account/Login and logout related
     */
    public SelenideElement accountButton = $("a.skip-account");
    public SelenideElement accountLoginButton = $("#header-account a[title='Log In']");
    public SelenideElement loginEmail = $("#email.input-text.required-entry.validate-email");
    public SelenideElement loginPassword = $("#pass.input-text.required-entry.validate-password");
    public SelenideElement loginButton = $("#send2");
    public SelenideElement myAccount = $("#header-account a[title='My Account']");
    public SelenideElement logoutButton = $("#header-account a[title='Log Out']");
    public ElementsCollection registerButton = $$("button.button");
    public ElementsCollection editButton = $$("h3~a");

    /**
     * Cart
     */
    public SelenideElement cartButton = $(".header-minicart");
    public SelenideElement viewCart = $("a.cart-link");
    public SelenideElement emptyCartButton = $("button.button2.btn-empty");

    /**
     * Search
     */
    public SelenideElement searchEntry = $("#search.input-text.required-entry");
    public SelenideElement searchButton = $(".button.search-button");
    public ElementsCollection addToCartBtn = $$(".actions button");
    public ElementsCollection removeProductButtons = $$(".btn-remove.btn-remove2");
    public ElementsCollection wishListAddButtons = $$(".link-wishlist");
    public ElementsCollection wishListPageButton =
            $$("a[href='http://testfasttrackit.info/magento-test/wishlist/']");

    /**
     * Convenient Functions
     */
    public void Login ()
    {
        accountButton.click();
        accountLoginButton.click();
        loginEmail.sendKeys("csikibotondi@gmail.com");
        loginEmail.shouldHave(attribute("value", "csikibotondi@gmail.com"));
        loginPassword.sendKeys("Test12345");
        loginButton.click();
        $(".header-language-background .welcome-msg").shouldHave(text("WELCOME, CSIKI BOTOND!"));
    }

    public void Logout()
    {
       accountButton.click();
       logoutButton.click();
        $(".page-title").shouldHave(text("YOU ARE NOW LOGGED OUT"));
    }

    public void Search(String text) {
        searchEntry.click();
        searchEntry.sendKeys(text);
        searchEntry.shouldHave(attribute("value", text));
        searchButton.click();
        $(".page-title").shouldHave(text(text));

    }

    public void EmptyTheCart() {
        cartButton.click();
        viewCart.click();
        emptyCartButton.click();
        cartButton.click();
        $(".empty").shouldHave(text("You have no items in your shopping cart."));
    }
}
